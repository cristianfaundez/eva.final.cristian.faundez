<%-- 
    Document   : diccionario
    Created on : 01-11-2020, 18:55:32
    Author     : crist
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>


    <body class="text-center" >

        <h1>Buscar Palabra</h1>

        <title>Buscar Palabra</title>
        <form name="form" action="IngresoPalabraController" method="POST">  

            <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">

                <div class="form-group">
                    <label for="palabra">Buscar</label>
                    <input  name="palabra" class="form-control" required id="palabra" >

                </div>   

                <button type="submit" name="accion" value="buscarPalabra" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">Buscar</button>


        </form>
    </body>


</html>
