<%-- 
    Document   : historial
    Created on : 01-11-2020, 15:25:26
    Author     : crist
--%>

<%@page import="java.util.Iterator"%>
<%@page import="cl.entities.Diccionario"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Diccionario> palabras = (List<Diccionario>) request.getAttribute("palabras");
    Iterator<Diccionario> itDiccionario = palabras.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Registro de Hardware</title>
    </head>
    <body class="text-center" >


        <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
            <table border="1">
                <thead>
                <th>Codigo</th>
                <th>Palabra </th>
                <th>Fecha</th>

                </thead>
                <tbody>
                    <%while (itDiccionario.hasNext()) {
                            Diccionario hard = itDiccionario.next();%>
                    <tr>
                        <td><%= hard.getCodigo()%></td>
                        <td><%= hard.getPalabra()%></td>
                        <td><%= hard.getFecha()%></td>

                    </tr>
                    <%}%>                
                </tbody>           
            </table>

    </body>
</html>

