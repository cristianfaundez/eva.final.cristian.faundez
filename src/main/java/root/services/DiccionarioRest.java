/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

import cl.dao.DiccionarioJpaController;
import cl.entities.Diccionario;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/diccionario")
public class DiccionarioRest {
    
      EntityManagerFactory emf = Persistence.createEntityManagerFactory("diccionarioDS");
    EntityManager em;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {
       
         /*  em = emf.createEntityManager();

        List<Diccionario> lista = em.createNamedQuery("Diccionario.findAll").getResultList(); */
         
         DiccionarioJpaController dao = new DiccionarioJpaController();
         List<Diccionario> lista = dao.findDiccionarioEntities();
         
                 
        return Response.ok(200).entity(lista).build();
    }
    
    @GET
    @Path("/{palabra}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscar(@PathParam("palabra") String palabra) {
        Client client = ClientBuilder.newClient();
        
        WebTarget myResource = client.target("https://od-api.oxforddictionaries.com/api/v2/entries/es/" + palabra + "?fields=definitions");
        
        Definicion result = myResource
                .request(MediaType.APPLICATION_JSON)
                .header("app_id", "71331eb0")
                .header("app_key", "cd1e7168655e364bd60a26c045eff2ce")
                .get(Definicion.class);
        
        List<String> definitions = new ArrayList<>();
     
        List<Result> results = result.getResults();
        for(Result r : results) {
            for (LexicalEntry lexicalEntry: r.getLexicalEntries()) {
                for (Entry entry : lexicalEntry.getEntries()) {
                    for (Sense sense : entry.getSenses()) {
                        for(String definition: sense.getDefinitions()) {
                            definitions.add(definition);
                        }
                    }
                }
            }
        }

        
      return Response.ok(200).entity(definitions).build();
  
        
    } 
   
    @POST
     @Produces(MediaType.APPLICATION_JSON)
     public String nuevo(Diccionario diccionario) {
         DiccionarioJpaController dao = new DiccionarioJpaController();
         try {
            dao.create(diccionario);
         } catch (Exception ex) {
             Logger.getLogger(DiccionarioRest.class.getName()).log(Level.SEVERE, null, ex);            
         }
         return "Busqueda almacenada";
     }
    
}
