
package root.services;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Definicion {

private List<Result> results = null;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* No args constructor for use in serialization
*
*/
public Definicion() {
}

/**
*
* @param results
*/
public Definicion(List<Result> results) {
super();
this.results = results;
}

public List<Result> getResults() {
return results;
}

public void setResults(List<Result> results) {
this.results = results;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}





