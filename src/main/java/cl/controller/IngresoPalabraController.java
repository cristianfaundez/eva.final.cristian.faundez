/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller;


import cl.dao.DiccionarioJpaController;
import cl.entities.Diccionario;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;



/**
 *
 * @author crist
 */
@WebServlet(name = "IngresoPalabraController", urlPatterns = {"/IngresoPalabraController"})
public class IngresoPalabraController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet IngresoPalabraController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet IngresoPalabraController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("accion");

        if (action.equals("buscar"))  {
            request.getRequestDispatcher("diccionario.jsp").forward(request, response);
            return;
        }
        if (action.equals("buscarPalabra")) {
            String palabra =request.getParameter("palabra");
            Date fecha = new Date();
            Diccionario diccionario = new Diccionario();
            diccionario.setFecha(fecha);
            diccionario.setCodigo(""+ fecha.getTime());
            diccionario.setPalabra(palabra);
            
            DiccionarioJpaController dao = new DiccionarioJpaController();
            try {
                dao.create(diccionario);
            } catch (Exception ex) {
                Logger.getLogger(IngresoPalabraController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            Client cliente = ClientBuilder.newClient();
            WebTarget resource = cliente.target("http://DESKTOP-THK7JD9:8081/eva_final_cristian_faundez-1.0-SNAPSHOT/api/diccionario/"+ palabra);
            List<String> definiciones = resource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<String>>(){});
            request.setAttribute("definiciones", definiciones);
            request.getRequestDispatcher("definicion.jsp").forward(request, response);
            
            return;
           
        }

        Client client = ClientBuilder.newClient();

        WebTarget myResource = client.target("http://DESKTOP-THK7JD9:8081/eva_final_cristian_faundez-1.0-SNAPSHOT/api/diccionario");

        List<Diccionario> palabra = myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Diccionario>>() {
        });

        request.setAttribute("palabras", palabra);
        request.getRequestDispatcher("historial.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
